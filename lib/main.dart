import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String thaiGreeting = "สวัสดี Flutter";
String chineseGreeting = "你好 Flutter";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishGreeting?
                    spanishGreeting: englishGreeting;
              });
            },
                icon: Icon(Icons.create_rounded)),
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishGreeting?
                  thaiGreeting: englishGreeting;
              });
            },
                icon: Icon(Icons.calendar_view_day)),
            IconButton(onPressed: () {
              setState(() {
                displayText = displayText == englishGreeting?
                  chineseGreeting: englishGreeting;
              });
            },
                icon: Icon(Icons.discord))
          ],
        ),
        body: Center (
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24),
          )
        ),
      ),
    );
  }
}
//SafeArea Widget
// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       //Scaffold Widget
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center (
//           child: Text(
//             "Hello Flutter !",
//             style: TextStyle(fontSize: 24),
//           )
//         ),
//       ),
//     );
//   }
//
// }